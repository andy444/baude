from .models import SiteInfo

def get_site_info(request):
    return {'site': SiteInfo.objects.all().first()}