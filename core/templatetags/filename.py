from django import template

register = template.Library()

@register.filter
def filename(value):
    """Gets only filename"""
    return value.split('/')[-1]
