from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class MenuCategory(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Menu Categories'


class MenuItem(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    ingredients = models.TextField(blank=True) 
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.ImageField(upload_to='menu', blank=True)
    portion_size = models.CharField(max_length=8, null=True)
    category = models.ForeignKey(MenuCategory,
                                 null=True,
                                 on_delete=models.SET_NULL)

    def __str__(self):
        return self.title


class Room(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='rooms')
    description = RichTextUploadingField()
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return self.title


class Message(models.Model):
    MESSAGE_TYPES = [
        ('ALERT', 'Alarm'),
        ('WARNING', 'Warnung'),
        ('INFO', 'Info'),
    ]

    title = models.CharField(max_length=150)
    content = models.TextField()
    message_type = models.CharField(max_length=10,
                                    choices=MESSAGE_TYPES,
                                    default='INFO')


class TravelGuide(models.Model):
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to='travel')
    # short_description
    content = RichTextUploadingField()


class SiteInfo(models.Model):
    name = models.CharField(max_length=150)
    header_image = models.ImageField(upload_to='site')
    address = models.TextField()
    telephone = models.CharField(max_length=15)
    email = models.EmailField()
    welcome_text = RichTextUploadingField()
    arriving = RichTextUploadingField()
    about_us = RichTextUploadingField()
    intro_menu = models.TextField(blank=True)
    pdf_menu = models.FileField(upload_to='site', null=True)
    intro_travel = models.TextField(blank=True)
    impressum = RichTextUploadingField(blank=True)
    datenschutz = RichTextUploadingField(blank=True)

    class Meta:
        verbose_name_plural = 'Site Info'
