from django.contrib import admin
from .models import MenuCategory, MenuItem, SiteInfo, Room, Message, TravelGuide


class SiteInfoAdmin(admin.ModelAdmin):

    list_display = ('name',)

    def has_add_permission(self, request):
        if self.model.objects.count() >= 1:
            return False
        return super().has_add_permission(request)


class MessageAdmin(admin.ModelAdmin):

    list_display = ('title', 'message_type')
    list_filter = ('message_type',)


class RoomAdmin(admin.ModelAdmin):

    list_display = ('title', 'price')


class MenuItemAdmin(admin.ModelAdmin):

    list_display = ('title', 'price', 'portion_size', 'category')
    list_filter = ('price', 'category')


class TravelGuideAdmin(admin.ModelAdmin):

    list_display = ('title',)


admin.site.register(SiteInfo, SiteInfoAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(MenuCategory)
admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(TravelGuide, TravelGuideAdmin)
