from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('rooms/', views.RoomsView.as_view(), name='rooms'),
    path('menu/', views.MenuView.as_view(), name='menu'),
    path('about/', views.AboutView.as_view(), name='about'),
    path('arriving/', views.ArrivingView.as_view(), name='arriving'),
    path('travel/', views.GuideView.as_view(), name='travel'),
    path('impressum/', views.ImpressumView.as_view(), name='impressum'),
    path('datenschutz/', views.DatenschutzView.as_view(), name='datenschutz'),
]
