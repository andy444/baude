from django.views.generic import ListView, TemplateView
from .models import Room, MenuItem, TravelGuide, Message, MenuCategory


class HomeView(TemplateView):
    template_name = 'core/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['messages'] = Message.objects.all()
        return context


class RoomsView(ListView):
    template_name = 'core/rooms.html'
    model = Room
    context_object_name = 'rooms'


class MenuView(ListView):
    template_name = 'core/menu.html'
    model = MenuItem
    context_object_name = 'menu_items'
    ordering = ['category']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = MenuCategory.objects.all()
        return context


class GuideView(ListView):
    template_name = 'core/travel.html'
    model = TravelGuide
    context_object_name = 'travel_guides'


class AboutView(TemplateView):
    template_name = 'core/about_us.html'


class ArrivingView(TemplateView):
    template_name = 'core/arriving.html'


class ImpressumView(TemplateView):
    template_name = 'core/impressum.html'


class DatenschutzView(TemplateView):
    template_name = 'core/datenschutz.html'
