# Generated by Django 3.2.5 on 2021-07-23 10:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20210723_1201'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menuitem',
            name='ingredients',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='menuitem',
            name='portion',
            field=models.CharField(max_length=8, null=True),
        ),
    ]
